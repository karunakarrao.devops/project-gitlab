variable "web" {
  type = map(string)
  default = {
    "web_ami"            = "ami-0f403e3180720dd7e"
    "web_instance_type"  = "t2.micro"
    "web_instance_count" = 1
    "web_region"         = "us-east-1"
  }
}