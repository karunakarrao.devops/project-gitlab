#  create an EC2 Instance 

resource "aws_instance" "web" {

  ami           = var.web[web_amiami]
  instance_type = var.web[web_instance_type]
  count         = var.web[web_instance_count]

  tags = {
    "Name" = "web-server-${count.index}"
    "Env"  = "DEV"
  }

  key_name        = aws_key_pair.local.key_name
  security_groups = [aws_security_group.web_ssh_http_sg.name]
}

resource "null_resource" "hosts" {

  depends_on = [aws_instance.web]
  triggers = {
    "time" = "${timestamp()}"
  }
  count = length(aws_instance.web)
  provisioner "local-exec" {
    command = "echo ${element(aws_instance.web[*].public_ip, count.index)} >> ./hosts"
    when    = create
  }
  provisioner "local-exec" {
    command = "rm -rf ./hosts"
    when    = destroy
  }
}
